# 7Bits Hybrid App

This is the 7Bits mobile hybrid app for users that want to get smarter. It doesn't contain any admin functionality for companies who want to administrate users they bought licenses for. 

## Build, run and deploy

This app is built with the [Ionic Framework](http://ionicframework.com/). You need to download and install their [CLI](http://ionicframework.com/docs/cli/) to run, develop and deploy the app. Currently supported target platforms are Android and iOS.

Ionic is based on Google's [AngularJS](https://angularjs.org/), a MVC javascript framework.

## Development

### Design

Uses the ionic framework. Individual styles can be found at /css.
*To do:* Use SCSS instead of CSS.

### Internationalization

We currently have a German and an English version of the app, the translation files can be found at /locales.

### JS files

Should be self explaining, except for:
- angular-translate.js: Translation library, currently doesn't work with bower for whatever reason
- angular-fire.js: Firebase library, currently doesn't work with bower for whatever reason
- firebase.js: Firebase, currently doesn't work with bower for whatever reason

## Database and backend

Currently done with [Firebase](http://firebase.com/). There is an official library for AngularJS that we're using heavily: [AngularFire](https://www.firebase.com/docs/web/libraries/angular/).

**Structure:**
- Codes: Companies can generate login-codes for their employees in order to sign up for the app.
- Companies: Holds all companies, provided by ID. Contains the company's users as well as an admin (a guy from the company that can administrate the users the company is paying for)
- Connected: Shows all currently logged in users/devices
- Content: Holds ratings and favorites of a content
- Superadmins: Holds administrators that are allowed to create/edit/delete whole companies = 7Bits staff
- Users: Holds data of users (this data is partially duplicating data of /companies/id/users, but that's best practice in Firebase as storage is cheap but power expensive)