var translation_de = {

	// #### GENERAL ####
	// Name of app
	APP_NAME: 'Moovery',

	// Back button in navbar
	BUTTON_BACK: '  Zurück',



	// #### SIGN IN SCREEN ####
	// Partially uses sign up variables

	// Email address input
	SIGN_IN_INPUT_EMAIL_ADDRESS: 'E-Mail Adresse',

	// Password input
	SIGN_IN_INPUT_PASSWORD: 'Passwort',

	// Sign in button
	SIGN_IN_BUTTON_SIGN_IN: 'Einloggen',

	// Sign up link
	SIGN_IN_LINK_SIGN_UP: 'Registrieren',

	// Forgot password link
	SIGN_IN_LINK_FORGOT_PASSWORD: 'Passwort vergessen',

	// Pop-up message if failure: No user name provided - title
	// Due to disabled button this should never trigger
	SIGN_IN_POPUP_NO_USERNAME_PROVIDED_TITLE: 'Uh',

	// Pop-up message if failure: No user name provided - content
	// Due to disabled button this should never trigger
	SIGN_IN_POPUP_NO_USERNAME_PROVIDED_CONTENT: 'Es ist ziemlich schwer für uns, wenn du keine E-Mail-Adresse angibst :/. ',

	// Pop-up message if failure: No password provided - title
	// Due to disabled button this should never trigger
	SIGN_IN_POPUP_NO_PASSWORD_PROVIDED_TITLE: 'Uh',

	// Pop-up message if failure: No password provided - content
	// Due to disabled button this should never trigger
	SIGN_IN_POPUP_NO_PASSWORD_PROVIDED_CONTENT: 'Es ist ziemlich schwer für uns, wenn du kein Passwort eingibst :/.',

	// Pop-up message if failure: Invalid email address - title
	SIGN_IN_POPUP_FAILURE_INVALID_EMAIL_TITLE: 'Uh',

	// Pop-up message if failure: Invalid email address - content
	SIGN_IN_POPUP_FAILURE_INVALID_EMAIL_CONTENT: 'Sieht so aus als ob diese E-Mail-Adresse nicht in unserem System ist, sorry :/.',

	// Pop-up message if failure: Invalid password - title
	SIGN_IN_POPUP_FAILURE_INVALID_PASSWORD_TITLE: 'Uh',

	// Pop-up message if failure: Invalid password - content
	SIGN_IN_POPUP_FAILURE_INVALID_PASSWORD_CONTENT: 'Sieht so aus als wäre dein Passwort falsch, sorry :/.',

	// Pop-up message if failure: Invalid user - title
	SIGN_IN_POPUP_FAILURE_INVALID_USER_TITLE: 'Uh',

	// Pop-up message if failure: Invalid user - content
	SIGN_IN_POPUP_FAILURE_INVALID_USER_CONTENT: 'Wir können deine E-Mail-Adresse nicht in unserer Datenbank finden, sorry :/.',



	// #### SIGN UP SCREEN ####
	// Partially uses sign in variables

	// Title in navbar
	SIGN_UP_NAVBAR_TITLE: 'Willkommen',

	// Confirm password input
	SIGN_UP_INPUT_PASSWORD_CONFIRM: 'Passwort bestätigen',

	// Sign up button
	SIGN_UP_BUTTON_SIGN_UP: 'Registrieren',

	// Confirmation pop-up for email address: headline
	SIGN_UP_POPUP_TITLE: 'Sicher?',

	// Confirmation pop-up for email address: content
	// Split for technical reasons: The provided email address is printed in between those
	// It will look like this: sign_up_popup_content_1 + mail address + sign_up_popup_content_2
	SIGN_UP_POPUP_CONTENT_1: 'Auch wenn wir uns anhören wie deine Mutter: Bist du sicher dass <strong>',
	SIGN_UP_POPUP_CONTENT_2: '</strong> richtig ist?',

	// Confirmation pop-up for email address: cancel button text
	SIGN_UP_POPUP_CANCEL: 'Nein',
	
	// Confirmation pop-up for email address: confirm button text
	SIGN_UP_POPUP_CONFIRM: 'Total',

	// Pop-up message if successfully registered
	SIGN_UP_POPUP_SUCCESS: 'Na dann... willkommen!',

	// Pop-up message if failure: Email already in use - title
	SIGN_UP_POPUP_FAILURE_EMAIL_TAKEN_TITLE: 'Hier warst du schon.',

	// // Pop-up message if failure: Email already in use - content
	// Split for technical reasons: The provided email address is printed in between those
	// It will look like this: SIGN_UP_POPUP_FAILURE_EMAIL_TAKEN_CONTENT_1 + SIGN_UP_POPUP_FAILURE_EMAIL_TAKEN_CONTENT_2
	SIGN_UP_POPUP_FAILURE_EMAIL_TAKEN_CONTENT_1: 'Sieht so aus als ob <strong>',
	SIGN_UP_POPUP_FAILURE_EMAIL_TAKEN_CONTENT_2: '</strong> bereits registriert ist.',

	// Pop-up message if failure: Email already in use - title
	SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_TITLE: 'Uh',

	// // Pop-up message if failure: Email invalid - content
	// Split for technical reasons: The provided email address is printed in between those
	// It will look like this: SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_CONTENT_1 + SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_CONTENT_2
	SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_CONTENT_1: 'Auf den zweiten Blick sieht <strong>',
	SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_CONTENT_2: '</strong> doch nicht wie eine gültige E-Mail-Adresse aus :(.<br /><br /> Bitte verwende eine andere Adresse.',

	// Pop-up message if failure: Network error - title
	SIGN_UP_POPUP_FAILURE_NETWORK_ERROR_TITLE: 'Uh',

	// Pop-up message if failure: Network error - content
	SIGN_UP_POPUP_FAILURE_NETWORK_ERROR_CONTENT: 'Sieht so aus als wärst du offline :(. <br /><br /> Ohne Internet-Verbindung ist es wirklich schwer für uns, kannst du dich bitte mit dem Internet verbinden?',

	// Pop-up message if failure: Default message that appears if error couldn't be identified - title
	SIGN_UP_POPUP_FAILURE_DEFAULT_TITLE: 'Uh!',

	// Pop-up message if failure: Default message that appears if error couldn't be identified - title
	SIGN_UP_POPUP_FAILURE_DEFAULT_CONTENT: 'Irgendetwas lief schief. Wir sind beschämt :(. <br /> <br /> Bitte versuche es nochmals. Danke!',



	// #### FORGOT PASSWORD SCREEN ####
	// Partially uses sign in variables

	// Title in navbar
	FORGOT_PASSWORD_NAVBAR_TITLE: 'Passwort vergessen',

	// Funny text
	FORGOT_PASSWORD_FUNNY_TEXT: 'Eine tägliche Portion Avocados <br />hat sehr postive Auswirkungen <br />auf das menschliche Gehirn :).',

	// Reset button
	FORGOT_PASSWORD_BUTTON_RESET: 'Anfordern',

	// Pop-up message if password successfully reset: Title
	FORGOT_PASSWORD_POPUP_SUCCESS_TITLE: 'Juhu',

	// Pop-up message if password successfully reset: Content
	// Split for technical reasons: The provided email address is printed in between those
	// It will look like this: FORGOT_PASSWORD_POPUP_SUCCESS_CONTENT_1 + FORGOT_PASSWORD_POPUP_SUCCESS_CONTENT_2
	FORGOT_PASSWORD_POPUP_SUCCESS_CONTENT_1: 'Sieh dir die Mailbox von <br />  <strong>',
	FORGOT_PASSWORD_POPUP_SUCCESS_CONTENT_2: '</strong> an! <br /> Du kannst dein Passwort dort zurücksetzen.',



	// #### SIDE MENU LIST ####
	// Just the headlines in the list, not the content that appears if one clicks them

	// Home
	MENU_LIST_HOME: 'Home',

	// Settings
	MENU_LIST_SETTINGS: 'Einstellungen',

	// Workplace setup
	MENU_LIST_WORKPLACE_SETUP: 'Arbeitsplatz einrichten',

	// About us
	MENU_LIST_ABOUT_US: 'Über uns',
	
	// Contact
	MENU_LIST_CONTACT: 'Kontakt',
	
	// Sign out
	MENU_LIST_SIGN_OUT: 'Ausloggen',



	// #### SETTINGS ####
	// Title in navbar
	SETTINGS_NAVBAR_TITLE: 'Einstellungen',

	// Title of divider element for bundling settings with similar functionality - 'Exercises'
	SETTINGS_DIVIDER_EXERCISES: 'Übungen',

	// Amount of exercise sessions per day
	SETTINGS_AMOUNT_EXERCISES: 'Übungen pro Tag',

	// Toggle button to switch notifications on
	SETTINGS_NOTIFICATIONS: 'Benachrichtigungen',

	// Subtext of notification, explains what happens if switched on
	SETTINGS_NOTIFICATIONS_SUBTEXT: 'Push-Nachrichten am Telefon',

	// Time of notification
	SETTINGS_NOTIFICATIONS_TIME: 'Erste Benachrichtigung',

	// Time of notification 2, just visible if amount of exercises per day is 2 as well
	SETTINGS_NOTIFICATIONS_TIME_2: 'Zweite Benachrichtigung',

	// Toggle button to switch email notifications on
	SETTINGS_NOTIFICATIONS_EMAIL: 'Email',

	// Subext of email notifications, explains what happens if switched on
	SETTINGS_NOTIFICATIONS_EMAIL_SUBTEXT: 'Auch per E-Mail benachrichtigen',

	// Title of divider element for bundling settings with similar functionality
	SETTINGS_DIVIDER_BASICS: 'Basics',

	// Title of language choice
	SETTINGS_BASICS_LANGUAGE: 'Sprache',

	// Language: English
	SETTINGS_BASICS_LANGUAGE_ENGLISH: 'English',

	// Language: German
	SETTINGS_BASICS_LANGUAGE_GERMAN: 'Deutsch',

	// Link to change mail address
	SETTINGS_BASICS_CHANGE_MAIL: 'E-Mail ändern',

	// Link to change password site
	SETTINGS_BASICS_CHANGE_PASSWORD: 'Passwort ändern',



	// #### CHANGE MAIL SCREEN ####
	// Title in navbar
	CHANGE_MAIL_NAVBAR_TITLE: 'E-Mail ändern',

	// 'New mail address' in input field
	CHANGE_EMAIL_INPUT_NEW_EMAIL: 'Neue E-Mail Adresse',

	// 'Password' in input field
	CHANGE_EMAIL_INPUT_PASSWORD: 'Passwort',

	// 'Change' button
	CHANGE_EMAIL_BUTTON_CHANGE: 'Wechseln',

	// Pop-up message if failure: Wrong password - title
	CHANGE_MAIL_POPUP_FAILURE_WRONG_PASSWORT_TITLE: 'Uh!',

	// Pop-up message if failure: Wrong password - content
	CHANGE_MAIL_POPUP_FAILURE_WRONG_PASSWORT_CONTENT: 'Das Passwort ist leider falsch :/. Wir benötigen das aus Sicherheitsgründen, tut uns leid. ',

	// Pop-up message if failure: Default message that appears if error couldn't be identified - title
	CHANGE_MAIL_POPUP_FAILURE_DEFAULT_TITLE: 'Uh!',

	// Pop-up message if failure: Default - content
	CHANGE_MAIL_POPUP_FAILURE_DEFAULT_ERROR: 'Irgendetwas lief schief. Wir sind beschämt :(. <br /> <br /> Bitte versuche es nochmals. Danke!',

	// Pop-up message if success: Success message that appears after changing mail address - title
	CHANGE_MAIL_POPUP_SUCCESS_CHANGE_MAIL_TITLE: 'Juhu!',

	// Pop-up message if success: Success message that appears after changing mail address - content
	CHANGE_MAIL_POPUP_SUCCESS_CHANGE_MAIL_CONTENT: 'Deine Mailadresse wurde geändert :).',

	

	// #### CHANGE PASSWORD SCREEN ####
	// Partially uses sign in variables
	// Partially uses sign up variables

	// Title in navbar
	CHANGE_PASSWORD_NAVBAR_TITLE: 'Passwort ändern',

	// 'Old password' in input
	CHANGE_PASSWORD_INPUT_OLD_PASSWORD: 'Altes Passwort',

	// 'New password' in input
	CHANGE_PASSWORD_INPUT_NEW_PASSWORD: 'Neues Passwort',

	// Confirm new password
	CHANGE_PASSWORD_INPUT_CONFIRM_PASSWORD: 'Neues Passwort bestätigen',

	// Save button
	CHANGE_PASSWORD_BUTTON_SAVE: 'Speichern',



	// #### ABOUT US SCREEN ####
	// Title in navbar
	ABOUT_US_NAVBAR_TITLE: 'Über uns',

	// Content of about us page
	ABOUT_US_CONTENT: 'Dies ist Version 0.1.0.',



	// #### CONTACT SCREEN ####
	// Title in navbar
	CONTACT_NAVBAR_TITLE: 'Kontakt',

	// Content of contact page
	CONTACT_CONTENT: 'Dies ist die Kontakt-Seite.',



	// #### SIGN OUT SCREEN ####
	// Title in navbar
	SIGN_OUT_NAVBAR_TITLE: 'Ausloggen',

	// Content of sign out page
	SIGN_OUT_CONTENT: 'Es ist wohl keine große Überraschung dass <br />du dich wieder einloggen musst, wenn du <br />dich jetzt ausloggst, oder?<br /> <br />Wir empfehlen es nicht, aber es ist eine freie Welt :).',

	// Log out button
	SIGN_OUT_BUTTON_LOG_OUT: 'Ausloggen',



	// #### HOME SCREEN ####
	// Headline of first third = Office exercise section
	HOME_OFFICE_EXERCISES: 'Büro Übungen',

	// Current level of office exercise
	HOME_OFFICE_EXERCISES_LEVEL: 'Level 1',

	// Headline of second third = Instant exercise section
	HOME_INSTANT_EXERCISE: 'Einzelne Übung',

	// Headline of second third = Statistics section
	HOME_STATISTICS: 'Statistik',

	// Current level of office exercise
	HOME_STATISTICS_LEVEL: 'Novitze',



	// #### OFFICE EXERCISE OVERVIEW SCREEN ####
	// Partially uses home_office_exercises variables

	// Title in navbar
	OFFICE_EXERCISE_OVERVIEW_NAVBAR_TITLE: 'Büro Übungen',

	// Today's focus of exercises
	OFFICE_EXERCISE_OVERVIEW_HEADLINE: 'Heute: Koordination.',

	// Headline of the list of today's exercises
	OFFICE_EXERCISE_OVERVIEW_EXERCISES: 'Übungen',

	// Headline of description why today's exercises make sense
	OFFICE_EXERCISES_OVERVIEW_PURPOSE: 'Warum?',

	// Description of why today's exercises make sense
	OFFICE_EXERCISES_OVERVIEW_PURPOSE_DESCRIPTION: 'Diese Einheit reduziert signifikant das Risiko eines Bandscheiben-Vorfalls und bewahrt vor Schädigung der Augen.',



	// #### INSTANT EXERCISE CHOOSE SCREEN ####
	// Title in navbar
	INSTANT_EXERCISE_CHOICE_NAVBAR_TITLE: 'Auswählen',

	// Choose exercise: By body area
	INSTANT_EXERCISE_CHOICE_BODY_AREA: 'Nach Körperregion',

	// Choose exercises: By emotion
	INSTANT_EXERCISE_CHOICE_EMOTION: 'Nach Emotion',



	// #### INSTANT EXERCISE CHOOSE BODY AREA ####
	// Title in navbar
	INSTANT_EXERCISE_AREA_NAVBAR_TITLE: 'Körperregion',

	// Areas
	INSTANT_EXERCISE_AREA_EYES: 'Augen & Atmung',
	INSTANT_EXERCISE_AREA_ARMS: 'Arme',
	INSTANT_EXERCISE_AREA_TRUNK: 'Rumpf',
	INSTANT_EXERCISE_AREA_LEGS: 'Beine',

	// EYES & BREATHING
	// Title in navbar
	INSTANT_EXERCISE_AREA_EYES_NAVBAR_TITLE: 'Augen & Atmung',

	// Content
	INSTANT_EXERCISE_AREA_EYES_STEP_1: 'Schritt 1',
	INSTANT_EXERCISE_AREA_EYES_STEP_1_CONTENT: 'Dies ist die super coole Beschreibung des ersten Schritts.',
	INSTANT_EXERCISE_AREA_EYES_STEP_2: 'Schritt 2',
	INSTANT_EXERCISE_AREA_EYES_STEP_2_CONTENT: 'Dies ist die super coole Beschreibung des zweiten Schritts.',
	INSTANT_EXERCISE_AREA_EYES_STEP_3: 'Schritt 3',
	INSTANT_EXERCISE_AREA_EYES_STEP_3_CONTENT: 'Dies ist die super coole Beschreibung des dritten Schritts.',

	// ARMS
	// Title in navbar
	INSTANT_EXERCISE_AREA_ARMS_NAVBAR_TITLE: 'Arme',

	// TRUNK
	// Title in navbar
	INSTANT_EXERCISE_AREA_TRUNK_NAVBAR_TITLE: 'Rumpf',

	// LEGS
	// Title in navbar
	INSTANT_EXERCISE_AREA_LEGS_NAVBAR_TITLE: 'Beine',



	// #### INSTANT EXERCISE CHOOSE EMOTION ####
	// Title in navbar
	INSTANT_EXERCISE_EMOTION_NAVBAR_TITLE: 'Emotion',

	// Emotions
	INSTANT_EXERCISE_EMOTION_ENERGIZED: 'Voller Energie',
	INSTANT_EXERCISE_EMOTION_OK: 'Ganz ok',
	INSTANT_EXERCISE_EMOTION_TIRED: 'Müde',



	// #### STATISTICS SCREEN ####
	// Title in navbar
	STATISTICS_NAVBAR_TITLE: 'Statistik',

	// Title of 'me'-tab
	STATISTICS_TAB_ME: 'Ich',

	// Title of 'company'-tab
	STATISTICS_TAB_COMPANY: 'Unternehmen',

	// Headline for monthly statistics
	STATISTICS_MONTH: 'Monat',

	// Headline for yearly statistics
	STATISTICS_YEAR: 'Jahr',

	// Headline for overall statistics
	STATISTICS_OVERALL: 'Gesamt',

	// Headline for 'other statistics'
	STATISTICS_OTHER_STATISTICS: 'Andere Statistiken',

	// Headline for 'team effort'
	STATISTICS_TEAM_EFFORT: 'Team',

	// Headline for 'discipline'
	STATISTICS_DISCIPLINE: 'Disziplin',

	// Headline for 'eagerness'
	STATISTICS_EAGERNESS: 'Anstrengung'
}