var translation_en = {


	// #### GENERAL ####
	// Name of app
	APP_NAME: '7 Bits',

	// Back button in navbar
	BUTTON_BACK: '  Back',

	
	/*------------------------------------*\
		#  UTILITIES
	\*------------------------------------*/
	// #### SIGN IN OR SIGN UP CHOICE ####
	// Text of sign in button
	SIGN_IN_OR_UP_CHOICE_BUTTON_SIGN_UP: 'Sign up',

	// Text of sign in button
	SIGN_IN_OR_UP_CHOICE_BUTTON_SIGN_IN: 'Sign in',


	// #### SIGN IN SCREEN ####
	// Text of the four sign in possibilities
	SIGN_IN_WITH_CODE: 'Sign in with code.',
	SIGN_IN_WITH_MAIL: 'Sign in with email.',
	SIGN_IN_WITH_FACEBOOK: 'Sign in with facebook.',
	SIGN_IN_WITH_GOOGLE: 'Sign in with google.',

	// Text that asks user to type in password
	SIGN_IN_WITH_CODE_TYPE_IN_PASSWORD: 'Please type in your password: ',

	// Email address input
	SIGN_IN_INPUT_EMAIL_ADDRESS: 'Email address',

	// Password input
	SIGN_IN_INPUT_PASSWORD: 'Password',

	// Sign in button
	SIGN_IN_BUTTON_SIGN_IN: 'Sign in',

	// Sign up link
	SIGN_IN_LINK_SIGN_UP: 'Sign up',

	// Forgot password link
	SIGN_IN_LINK_FORGOT_PASSWORD: 'Forgot password',

	// Pop-up message if failure: No user name provided - title
	// Due to disabled button this should never trigger
	SIGN_IN_POPUP_NO_USERNAME_PROVIDED_TITLE: 'Oh no',

	// Pop-up message if failure: No user name provided - content
	// Due to disabled button this should never trigger
	SIGN_IN_POPUP_NO_USERNAME_PROVIDED_CONTENT: "It's really hard for us without you providing your mail address :/. ",

	// Pop-up message if failure: No password provided - title
	// Due to disabled button this should never trigger
	SIGN_IN_POPUP_NO_PASSWORD_PROVIDED_TITLE: 'Uh',

	// Pop-up message if failure: No password provided - content
	// Due to disabled button this should never trigger
	SIGN_IN_POPUP_NO_PASSWORD_PROVIDED_CONTENT: "It's really hard for us without you providing your password :/. ",

	// Pop-up message if failure: Invalid email address - title
	SIGN_IN_POPUP_FAILURE_INVALID_EMAIL_TITLE: 'Oh no',

	// Pop-up message if failure: Invalid email address - content
	SIGN_IN_POPUP_FAILURE_INVALID_EMAIL_CONTENT: "Looks like your email address isn't in our system, sorry :/.",

	// Pop-up message if failure: Invalid password - title
	SIGN_IN_POPUP_FAILURE_INVALID_PASSWORD_TITLE: 'Uh',

	// Pop-up message if failure: Invalid password - content
	SIGN_IN_POPUP_FAILURE_INVALID_PASSWORD_CONTENT: 'Looks like your password is wrong, sorry :/.',

	// Pop-up message if failure: Invalid user - title
	SIGN_IN_POPUP_FAILURE_INVALID_USER_TITLE: 'Uh',

	// Pop-up message if failure: Invalid user - content
	SIGN_IN_POPUP_FAILURE_INVALID_USER_CONTENT: "We can't find your mail address in our database, sorry :/.",

	// #### SIGN UP SCREEN ####
	// Partially uses sign in variables

	// Title in navbar
	SIGN_UP_NAVBAR_TITLE: 'Sign up',


	// #### SIGN UP SCREEN ####
	// Text of the four sign up possibilities
	SIGN_UP_WITH_CODE: 'Sign up with code.',
	SIGN_UP_WITH_MAIL: 'Sign up with email.',
	SIGN_UP_WITH_FACEBOOK: 'Sign up with facebook.',
	SIGN_UP_WITH_GOOGLE: 'Sign up with google.',

	// Intro question that asks for sign up code
	SIGN_UP_WITH_CODE_INTRO: 'Did your company provide you with a code?',

	// Grey-out text of code-input-field
	SIGN_UP_WITH_CODE_INPUT_CODE: 'Code',

	// Sign up button
	SIGN_UP_WITH_CODE_BUTTON_SIGN_UP: 'Sign up',

	// Pop-up text when code is invalid
	SIGN_UP_WITH_CODE_POPUP_FAILURE: 'Sorry, that code is invalid :(.',

	// Headline that confirms that code was valid
	SIGN_UP_WITH_CODE_SUCCESS_HEADLINE: 'Bingo!',

	// Text right before user's mail address is shown
	SIGN_UP_WITH_CODE_SUCCESS_MAIL_ADDRESS: 'Your mail address: ',

	// Text that asks user to choose a password
	SIGN_UP_WITH_CODE_SUCCESS_CHOOSE_PASSWORD: 'Please choose a password now:',

	// Welcome message in pop up that informs that user has been signed up
	SIGN_UP_WITH_CODE_POPUP_SUCCESS: 'Welcome!',

	// Confirm password input
	SIGN_UP_INPUT_PASSWORD_CONFIRM: 'Confirm password',

	// Sign up button
	SIGN_UP_BUTTON_SIGN_UP: 'Join',

	// Confirmation pop-up for email address: headline
	SIGN_UP_POPUP_TITLE: 'Sure?',

	// Confirmation pop-up for email address: content
	// Split for technical reasons: The provided email address is printed in between those
	// It will look like this: SIGN_UP_POPUP_CONTENT_1 + mail address + SIGN_UP_POPUP_CONTENT_2
	SIGN_UP_POPUP_CONTENT_1: "Jep we're almost as bad as your mum, but are you sure <strong>",
	SIGN_UP_POPUP_CONTENT_2: '</strong> is spelled right?',

	// Confirmation pop-up for email address: cancel button text
	SIGN_UP_POPUP_CANCEL: 'Cancel',
	
	// Confirmation pop-up for email address: confirm button text
	SIGN_UP_POPUP_CONFIRM: 'Totally',

	// Pop-up message if successfully registered
	SIGN_UP_POPUP_SUCCESS: 'Well then... welcome!',

	// Pop-up message if failure: Email already in use - title
	SIGN_UP_POPUP_FAILURE_EMAIL_TAKEN_TITLE: "You've been here.",

	// // Pop-up message if failure: Email already in use - content
	// Split for technical reasons: The provided email address is printed in between those
	// It will look like this: SIGN_UP_POPUP_FAILURE_EMAIL_TAKEN_CONTENT_1 + SIGN_UP_POPUP_FAILURE_EMAIL_TAKEN_CONTENT_2
	SIGN_UP_POPUP_FAILURE_EMAIL_TAKEN_CONTENT_1: 'Looks like <strong>',
	SIGN_UP_POPUP_FAILURE_EMAIL_TAKEN_CONTENT_2: '</strong> is already registered.',

	// Pop-up message if failure: Email already in use - title
	SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_TITLE: 'Damn it!',

	// // Pop-up message if failure: Email invalid - content
	// Split for technical reasons: The provided email address is printed in between those
	// It will look like this: SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_CONTENT_1 + SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_CONTENT_2
	SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_CONTENT_1: 'On second sight <strong>',
	SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_CONTENT_2: "</strong> doesn't look like a valid email address :(.<br /><br /> Please give us a different one.",

	// Pop-up message if failure: Network error - title
	SIGN_UP_POPUP_FAILURE_NETWORK_ERROR_TITLE: 'Uh!',

	// Pop-up message if failure: Network error - content
	SIGN_UP_POPUP_FAILURE_NETWORK_ERROR_CONTENT: "Looks like you're offline :(. <br /><br /> It's really hard for us without an internet connection, could you please turn it on?",

	// Pop-up message if failure: Default message that appears if error couldn't be identified - title
	SIGN_UP_POPUP_FAILURE_DEFAULT_TITLE: 'Uh!',

	// Pop-up message if failure: Default message that appears if error couldn't be identified - title
	SIGN_UP_POPUP_FAILURE_DEFAULT_CONTENT: 'Something went wrong. We feel ashamed :(. <br /> <br />Please try again. Thanks!',


	// #### FORGOT PASSWORD SCREEN ####
	// Partially uses sign in variables

	// Title in navbar
	FORGOT_PASSWORD_NAVBAR_TITLE: 'Forgot password',

	// Funny text
	FORGOT_PASSWORD_FUNNY_TEXT: "Eat more avocados, <br /> that's good for your brain :).",

	// Reset button
	FORGOT_PASSWORD_BUTTON_RESET: 'Reset',

	// Pop-up message if password successfully reset: Title
	FORGOT_PASSWORD_POPUP_SUCCESS_TITLE: 'Wohoo',

	// Pop-up message if password successfully reset: Content
	// Split for technical reasons: The provided email address is printed in between those
	// It will look like this: FORGOT_PASSWORD_POPUP_SUCCESS_CONTENT_1 + FORGOT_PASSWORD_POPUP_SUCCESS_CONTENT_2
	FORGOT_PASSWORD_POPUP_SUCCESS_CONTENT_1: 'Check out <strong>',
	FORGOT_PASSWORD_POPUP_SUCCESS_CONTENT_2: '</strong>. <br /> You can reset your password there.',


	// #### CHANGE MAIL SCREEN ####
	// Title in navbar
	CHANGE_MAIL_NAVBAR_TITLE: 'Change mail',

	// 'New mail address' in input field
	CHANGE_EMAIL_INPUT_NEW_EMAIL: 'New mail address',

	// 'Password' in input field
	CHANGE_EMAIL_INPUT_PASSWORD: 'Password',

	// 'Change' button
	CHANGE_EMAIL_BUTTON_CHANGE: 'Change',

	// Pop-up message if failure: Wrong password - title
	CHANGE_MAIL_POPUP_FAILURE_WRONG_PASSWORT_TITLE: 'Uh!',

	// Pop-up message if failure: Wrong password - content
	CHANGE_MAIL_POPUP_FAILURE_WRONG_PASSWORT_CONTENT: 'Sorry, the password is wrong :/. We need it for security reasons. ',

	// Pop-up message if failure: Default message that appears if error couldn't be identified - title
	CHANGE_MAIL_POPUP_FAILURE_DEFAULT_TITLE: 'Uh!',

	// Pop-up message if failure: Default - content
	CHANGE_MAIL_POPUP_FAILURE_DEFAULT_ERROR: 'Something went wrong. We feel ashamed :(. <br /> <br />Please try again. Thanks!',

	// Pop-up message if success: Success message that appears after changing mail address - title
	CHANGE_MAIL_POPUP_SUCCESS_CHANGE_MAIL_TITLE: 'Wohoo!',

	// Pop-up message if success: Success message that appears after changing mail address - content
	CHANGE_MAIL_POPUP_SUCCESS_CHANGE_MAIL_CONTENT: 'You changed your mail address :).',


	// #### CHANGE PASSWORD SCREEN ####
	// Partially uses sign in variables
	// Partially uses sign up variables

	// Title in navbar
	CHANGE_PASSWORD_NAVBAR_TITLE: 'Change Password',

	// 'Old password' in input
	CHANGE_PASSWORD_INPUT_OLD_PASSWORD: 'Old Password',

	// 'New password' in input
	CHANGE_PASSWORD_INPUT_NEW_PASSWORD: 'New Password',

	// Confirm new password
	CHANGE_PASSWORD_INPUT_CONFIRM_PASSWORD: 'Confirm Password',

	// Save button
	CHANGE_PASSWORD_BUTTON_SAVE: 'Save',


	// #### SIGN OUT SCREEN ####
	// Title in navbar
	SIGN_OUT_NAVBAR_TITLE: 'Sign Out',

	// Content of sign out page
	SIGN_OUT_CONTENT: "It is not a big suprise if we tell you that <br />you need to login again if you sign out now, right? <br /> So we don't recommend it, but it's a free world :).",

	// Sign out button
	SIGN_OUT_BUTTON_LOG_OUT: 'Sign out',


	// #### TRIAL ####

	// Pop up that informs user that he is on full-functional-trial
	TRIAL_POP_UP_GENERAL_INFO_TITLE: 'Welcome :).',
	TRIAL_POP_UP_GENERAL_INFO_CONTENT: 'You have full functionality for three days :). Go to the settings in order to buy a premium account.',


	/*------------------------------------*\
		#  SETTINGS
	\*------------------------------------*/

	// #### SIDE MENU LIST ####
	// Just the headlines in the list, not the content that appears if one clicks them

	// Home
	MENU_LIST_HOME: 'Home',

	// Account
	MENU_LIST_ACCOUNT: 'Account',

	// Settings
	MENU_LIST_SETTINGS: 'Settings',

	// Feedback & Contact
	MENU_LIST_FEEDBACK_CONTACT: 'Feedback & Contact',

	// Help center
	MENU_LIST_HELP_CENTER: 'Help center',
	
	// Privacy Policy
	MENU_LIST_PRIVACY_POLICY: 'Privacy Policy',
	
	// Sign out
	MENU_LIST_SIGN_OUT: 'Sign out',


	// #### ACCOUNT ####
	// Title in navbar
	ACCOUNT_NAVBAR_TITLE: 'Account',

	// Change password
	ACCOUNT_CHANGE_PASSWORD: 'Change Password',

	// Manage subscription
	ACCOUNT_MANAGE_SUBSCRIPTION: 'Manage Subscription',

	// Delete account
	ACCOUNT_DELETE_ACCOUNT: 'Delete Account',

	// Info about current subscription
	ACCOUNT_SUBSCRIPTION_INFO_TRIAL: 'You currently have a <strong>trial</strong> subscription.',
	ACCOUNT_SUBSCRIPTION_INFO_PREMIUM: 'You currently have a <strong>premium</strong> subscription.',
	ACCOUNT_SUBSCRIPTION_INFO_COMPANY: 'You currently have a <strong>company sponsored</strong> subscription.',

	// Subscription has expired
	ACCOUNT_SUBSCRIPTION_EXPIRY_EXPIRED: "Your subscription has expired :(. <br />You're now on a free trial again.",

	// First part of sentence that says 'Your subscription will expire in X days.'
	// Make sure NOT to put in the 'in' of 'in X days'
	ACCOUNT_SUBSCRIPTION_EXPIRY_EXPIRY_IN: 'Your subscription will expire ',

	// Text of 'Get premium account'-button
	//ACCOUNT_SUBSCRIPTION_BUTTON_GET_PREMIUM: 'Get premium account',

	// Text of 'Type in company code'-button
	ACCOUNT_SUBSCRIPTION_BUTTON_COMPANY_CODE: 'Type in company code',

	// Intro above the input field where customers can type in company code
	ACCOUNT_SUBSCRIPTION_UNLOCK_WITH_CODE_INTRO: 'Did your company provide you with a code? <br />Enter it here to unlock a premium account: ',

	// Placeholder text of code-input field
	ACCOUNT_SUBSCRIPTION_UNLOCK_WITH_CODE_INPUT_PLACEHOLDER: 'Code',

	// Text of verify button
	ACCOUNT_SUBSCRIPTION_UNLOCK_WITH_CODE_BUTTON_VERIFY: 'Verify',

	// #### FEEDBACK & CONTACT ####
	// Title in navbar
	FEEDBACK_CONTACT_NAVBAR_TITLE: 'Feedback & Contact',

	// Request a topic
	FEEDBACK_CONTACT_REQUEST_TOPIC: 'Request a topic',

	// General feedback
	FEEDBACK_CONTACT_GENERAL_FEEDBACK: 'General feedback', 


	// #### HELP CENTER ####
	// Title in navbar
	HELP_CENTER_NAVBAR_TITLE: 'Help Center',	


	// #### PRIVACY POLICY ####
	// Title in navbar
	PRIVACY_POLICY_NAVBAR_TITLE: 'Privacy Policy',					
							

	// #### SETTINGS ####
	// Title in navbar
	SETTINGS_NAVBAR_TITLE: 'Settings',

	// Title of divider element for bundling settings with similar functionality - 'Welcome screen'
	SETTINGS_DIVIDER_WELCOME_SCREEN: 'Welcome screen',

	// Amount of exercise sessions per day
	SETTINGS_WELCOME_SCREEN_MY_BITS: 'My bits',

	// Amount of exercise sessions per day
	SETTINGS_WELCOME_SCREEN_FAVORITES: 'Favorites',	

	// Amount of exercise sessions per day
	SETTINGS_WELCOME_SCREEN_LAST_VIEWED: 'Last viewed',

	// Toggle button to switch notifications on
	SETTINGS_NOTIFICATIONS: 'Notifications',

	// Subtext of notification, explains what happens if switched on
	SETTINGS_NOTIFICATIONS_SUBTEXT: 'Push notifications on your phone',

	// Toggle button to switch email notifications on
	SETTINGS_NOTIFICATIONS_EMAIL: 'Email',

	// Subext of email notifications, explains what happens if switched on
	SETTINGS_NOTIFICATIONS_EMAIL_SUBTEXT: 'Send me an email too',

	// Title of divider element for bundling settings with similar functionality
	SETTINGS_DIVIDER_BASICS: 'Basics',

	// Title of language choice
	SETTINGS_BASICS_LANGUAGE: 'Language',

	// Language: English
	SETTINGS_BASICS_LANGUAGE_ENGLISH: 'English',

	// Language: German
	SETTINGS_BASICS_LANGUAGE_GERMAN: 'Deutsch',

	// Toggle button to stay logged in
	SETTINGS_STAY_LOGGED_IN: 'Stay logged in',

	// Link to change mail address
	SETTINGS_BASICS_CHANGE_MAIL: 'Change mail',

	// Link to change password site
	SETTINGS_BASICS_CHANGE_PASSWORD: 'Change password',



	/*------------------------------------*\
		#  HOME
	\*------------------------------------*/

	// Headline of top bar
	HOME_TITLE: 'Library',

	// Titles of categories 
	HOME_LEADERSHIP: 'Leadership',
	HOME_COMMUNICATION: 'Communication',
	HOME_TEAM_WORK: 'Teamwork',
	HOME_PRESENTING: 'Presenting & Meetings',
	HOME_CONFLICT: 'Conflict',
	HOME_CUSTOMER_SUPPORT: 'Customer Service & Sales',
	HOME_EFFECTIVENESS: 'Personal Effectiveness',
	HOME_CHANGE: 'Change & Innovation',


	/*------------------------------------*\
		#  FAVORITES
	\*------------------------------------*/

	// Headline of top bar
	FAVORITES_TITLE: 'Favorites',


	/*------------------------------------*\
		#  RECENT
	\*------------------------------------*/

	// Headline of top bar
	RECENT_TITLE: 'Recent',


	/*------------------------------------*\
		#  SEARCH
	\*------------------------------------*/

	// Headline of top bar
	SEARCH_TITLE: 'Search',




	/*------------------------------------*\
		#  CATEGORIES
	\*------------------------------------*/

	// #### CATEGORY COMMUNICATION ####

	// Top bar title
	CATEGORY_COMMUNICATION_TITLE: 'Communication',
	
	// Category titles
	CATEGORY_COMMUNICATION_BEFORE_MAKING_A_COMMITMENT: 'Before Making a Commitment',
	CATEGORY_COMMUNICATION_GETTING_COMMITMENT: 'Getting Commitment',
	CATEGORY_COMMUNICATION_BREAKING_A_COMMITMENT_PROFESSIONALLY: 'Breaking a Commitment Professionally',
	CATEGORY_COMMUNICATION_COMMUNICATING_WITH_SOMEONE_WHO_BROKE_THEIR_COMMITMENT: 'Communicating with Someone Who Broke Their Commitment',
	CATEGORY_COMMUNICATION_GIVING_CONSTRUCTIVE_FEEDBACK: 'Giving Constructive Feedback',
	CATEGORY_COMMUNICATION_WRITING_CLEAR_AND_EFFICIENT_EMAIL: 'Writing Clear and Efficient Emails',
	CATEGORY_COMMUNICATION_ASKING_FOR_FEEDBACK: 'Asking for Feedback',
	CATEGORY_COMMUNICATION_DEALING_WITH_STRESS: 'Dealing with Stress/Performance Anxiety',
	CATEGORY_COMMUNICATION_PRESENTING_WITH_IMPACT: 'Presenting with Impact',
	CATEGORY_COMMUNICATION_ASKING_FOR_HELP: 'Asking for Help',
	CATEGORY_COMMUNICATION_STRUCTURING_WRITTEN_MESSAGES: 'Structuring Written Messages',
	CATEGORY_COMMUNICATION_EFFECTIVE_VERBAL_COMMUNICATION: 'Effective Verbal Communication',
	CATEGORY_COMMUNICATION_EXPRESSING_DISAGREEMENT: 'Expressing Disagreement',
						
	// #### CATEGORY LEADERSHIP ####
	// Headline of top bar
	CATEGORY_PAGE_COMMUNICATION_BEFORE_MAKING_A_COMMITMENT: 'Before Making a Commitment',
	CATEGORY_PAGE_COMMUNICATION_GETTING_COMMITMENT: 'Getting Commitment',
	CATEGORY_PAGE_COMMUNICATION_BREAKING_A_COMMITMENT_PROFESSIONALLY: 'Breaking a Commitment Professionally',
	CATEGORY_PAGE_COMMUNICATION_COMMUNICATING_WITH_SOMEONE_WHO_BROKE_THEIR_COMMITMENT: 'Communicating with someone who broke their Commitment',
	CATEGORY_PAGE_COMMUNICATION_GIVING_CONSTRUCTIVE_FEEDBACK: 'Giving constructive Feedback',
	CATEGORY_PAGE_COMMUNICATION_WRITING_CLEAR_AND_EFFICIENT_EMAIL: 'Writing Clear and Efficient Emails',
	CATEGORY_PAGE_COMMUNICATION_ASKING_FOR_FEEDBACK: 'Asking for Feedback',
	CATEGORY_PAGE_COMMUNICATION_DEALING_WITH_STRESS: 'Dealing with Stress/Performance Anxiety',
	CATEGORY_PAGE_COMMUNICATION_PRESENTING_WITH_IMPACT: 'Presenting with Impact',
	CATEGORY_PAGE_COMMUNICATION_ASKING_FOR_HELP: 'Asking for Help',
	CATEGORY_PAGE_COMMUNICATION_STRUCTURING_WRITTEN_MESSAGES: 'Structuring Written Messages',
	CATEGORY_PAGE_COMMUNICATION_EFFECTIVE_VERBAL_COMMUNICATION: 'Effective Verbal Communication',
	CATEGORY_PAGE_COMMUNICATION_EXPRESSING_DISAGREEMENT: 'Expressing Disagreement',

	COMMUNICATION_ASKING_FOR_FEEDBACK_HEADLINE_1: '1. It is inquiring into another person’s views and seeking to understand.',
	COMMUNICATION_ASKING_FOR_FEEDBACK_HEADLINE_2: '2. Inquire into other person(s) views.',
	COMMUNICATION_ASKING_FOR_FEEDBACK_HEADLINE_3: '3. Tell why you are asking the question.',
	COMMUNICATION_ASKING_FOR_FEEDBACK_HEADLINE_4: '4. Seek to understand.',
	COMMUNICATION_ASKING_FOR_FEEDBACK_HEADLINE_5: '5. Genuinely listen.',
	COMMUNICATION_ASKING_FOR_FEEDBACK_HEADLINE_6: '6. Be willing to experiment.',
	COMMUNICATION_ASKING_FOR_FEEDBACK_HEADLINE_7: '7. Think about how you will use the feedback?',

	COMMUNICATION_ASKING_FOR_FEEDBACK_TEXT_1: 'The basis for asking for all feedback is to find out about the other person(s) views and really show you are trying to understand their point of view.',
	COMMUNICATION_ASKING_FOR_FEEDBACK_TEXT_2: 'Begin by saying you want to gather some feedback and you would appreciate their view.',
	COMMUNICATION_ASKING_FOR_FEEDBACK_TEXT_3: 'Always explain ‘why’ you are asking for feedback in order to put context around the question.',
	COMMUNICATION_ASKING_FOR_FEEDBACK_TEXT_4: 'Really show that you are trying to understand their point of view, this will need to come from you and if face to face show you are listening via facial signals and tonality of your voice.',
	COMMUNICATION_ASKING_FOR_FEEDBACK_TEXT_5: 'This is the most important part. You have asked for feedback now it’s your chance to stay quiet and really listen to what they have to say, ‘actively listen’ instead of waiting to talk again!',
	COMMUNICATION_ASKING_FOR_FEEDBACK_TEXT_6: 'Sometimes the first try at inquiring and receiving feedback might not go exactly as planned so be ready to try again and show your willingness to understand the other person(s) point of view.',
	COMMUNICATION_ASKING_FOR_FEEDBACK_TEXT_7: 'Finally once you have received the feedback it is now time to decide how you want to use the feedback. After all receiving the feedback can often be easy but acting on the feedback to make a change is the part when the process will have been worthwhile.',

	// #### CATEGORY TEAMWORK ####

	// Top bar title
	CATEGORY_TEAMWORK_TITLE: 'Team work',

	// #### CATEGORY LEADERSHIP ####

	// Top bar title
	CATEGORY_LEADERSHIP_TITLE: 'Leadership',

	// #### CATEGORY EFFECTIVENESS ####

	// Top bar title
	CATEGORY_EFFECTIVENESS_TITLE: 'Effectiveness',

	// #### CATEGORY CUSTOMER SUPPORT ####

	// Top bar title
	CATEGORY_CUSTOMERSUPPORT_TITLE: 'Customer Support',

	// #### CATEGORY CHANGE ####

	// Top bar title
	CATEGORY_CHANGE_TITLE: 'Change',

	// #### CATEGORY CONFLICT ####

	// Top bar title
	CATEGORY_CONFLICT_TITLE: 'Conflict',

	// #### CATEGORY PRESENTING ####

	// Top bar title
	CATEGORY_PRESENTING_TITLE: 'Presenting',


	/*------------------------------------*\
		#  CONTENT
	\*------------------------------------*/
	
	CONTENT_FAVORITE_FAILURE_TOO_MANY_FAVORITES_TITLE: 'Too much :(',
	CONTENT_FAVORITE_FAILURE_TOO_MANY_FAVORITES_TEXT: "You shouldn't choose more than 7 favorites - please de-heart another one :).",


	/*------------------------------------*\
		#  FAVORITES
	\*------------------------------------*/

	// Text of delete button
	FAVORITES_BUTTON_DELETE: 'Delete',

	// Text of re-order button
	FAVORITES_BUTTON_REORDER: 'Reorder',

	// Message if there are no favorites yet 
	FAVORITES_NO_FAVORITES_YET: "You don't have any favorites yet. Click the heart icon at the top right to favor content!",



	/*------------------------------------*\
		#  SEARCH
	\*------------------------------------*/
	
	// Grey placeholder text of search input field
	SEARCH_SEARCH_INPUT_PLACEHOLDER: 'Search',
					
				
}