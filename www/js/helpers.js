var App = angular.module('ionicApp');

// Popup-alert service
App.factory("ShowAlert", function($ionicPopup) {
	return {

		// Shows an alert with ok-button
		alert: function(title, content){
			var alertPopup = $ionicPopup.alert({
				title: title,
				template: content,
				okType: 'button-balanced'
			});
		},

		// Shows an alert with ok and cancel button
		confirm: function(title, content, cancelText, okText){
			var confirmPopup = $ionicPopup.confirm({
				title: title,
				template: content,
				cancelText: cancelText,
				okText: okText,
				okType: 'button-balanced'
			});
			return confirmPopup;
		}
	};
});

// E-Mail validation service
App.directive('overwriteEmail', function() {
	var EMAIL_REGEXP = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	return {
		require: 'ngModel',
		restrict: '',
		link: function(scope, elm, attrs, ctrl) {

			// only apply the validator if ngModel is present and Angular has added the email validator
			if (ctrl && ctrl.$validators.email) {

				// this will overwrite the default Angular email validator
				ctrl.$validators.email = function(modelValue) {
					return ctrl.$isEmpty(modelValue) || EMAIL_REGEXP.test(modelValue);
				};
			}
		}
	};
});

// Input-match service
App.directive('match', function($parse) {
	return {
		require: 'ngModel',
		link: function(scope, elem, attrs, ctrl) {
			scope.$watch(function() {        
				return $parse(attrs.match)(scope) === ctrl.$modelValue;
			}, 
			function(currentValue) {
				ctrl.$setValidity('mismatch', currentValue);
			});
		}
	};
});

App.config(['$translateProvider', function ($translateProvider) {

	$translateProvider.determinePreferredLanguage();
	$translateProvider.fallbackLanguage("en");

	// Use variables from language js-files
	$translateProvider.translations('en', translation_en);
	$translateProvider.translations('de', translation_de);
}]);

// Translation: Automatically detect preferred language if mobile device
App.run(function($ionicPlatform, $translate) {
	$ionicPlatform.ready(function() {

		if(typeof navigator.globalization !== "undefined") {
			navigator.globalization.getPreferredLanguage(function(language) {
				$translate.use((language.value).split("-")[0])
				.then(function(data) {
					console.log("SUCCESS -> " + data);
				}, 
				function(error) {
					console.log("ERROR -> " + error);
				});
			}, null);
		}
	});
});

// Enable swipe + toggle for navigation menu
App.controller('NavCtrl', function($scope, $ionicSideMenuDelegate, $rootScope, ShowAlert, $translate) {

	$scope.showMenu = function () {
		$ionicSideMenuDelegate.toggleLeft();
	};
	$ionicSideMenuDelegate.canDragContent(true);
});

App.controller('languageSelectCtlr', function($scope, $translate){
	$scope.changeLanguage = function(){
		$translate.use($scope.languageSelectModel);
	};
});

App.controller('accordion', function($scope, $translate) {

	// Set initial view
	// This opens the very first accordion section
	$scope.view = 1;

	// Set with view = what accordion is shown
	$scope.setView = function(view){
		$scope.view = view;
	};
});

// Controller for every single content
// Handles favorites and rating
App.controller('ContentController', function($scope, $state, $rootScope, ShowAlert, $translate, $timeout) {

	var fullStateName = $state.current.name;

	// Remove 'tabs.'-prefix as it can't be stored in the database and therefore won't be there
	var stateName = fullStateName.replace('tabs.','');
		
	var favorites = new Firebase('https://7bits.firebaseio.com/users/' + $rootScope.userId + '/data/favorites');
	var list = [];

	favorites.on('value', function(snap) {

		var isFavored;
		list = snap.val();

		_.each(list, function(content){
			if(content.state === stateName){
				isFavored = true;
			}
		});

		if(!isFavored){
			$scope.isActive = false;
		}
		else {
			$scope.isActive = true;
		}
	});

	function addToFavorites(title){

		if(!list){
			list = [];
			list.push({'state': stateName, 'title': title});
		}
		else if(list.length > 7){
			ShowAlert.alert($translate.instant('CONTENT_FAVORITE_FAILURE_TOO_MANY_FAVORITES_TITLE'), $translate.instant('CONTENT_FAVORITE_FAILURE_TOO_MANY_FAVORITES_TEXT'));
		}
		else {
			list.push({'state': stateName, 'title': title});
		}
	}

	function removeFromFavorites(){

		// Get position of favorite in array
		var favoritePosition = _.findIndex(list, function(item){
			return item.state === stateName;
		});
		list.splice(favoritePosition, 1);
	}

	// Function to either add or remove content from favorites list
	$scope.favor = function(title){

		// Change class of button
		$scope.isActive = !$scope.isActive;

		// If content is now favored
		if($scope.isActive){
			addToFavorites(title);
		}
		else {
			removeFromFavorites();
		}
	};

	var recent = new Firebase('https://7bits.firebaseio.com/users/' + $rootScope.userId + '/data/recent');
	var recentContent = [];
	
	recent.on('value', function(snap) {

		recentContent = snap.val();

		// If content item is already in recent items, store position in array
		var alreadyInRecentPosition = _.findIndex(recentContent, function(content){
			return content.state === stateName;
		});

		// Wait for pagetitle to be resolved
		$timeout(function(){

			// If this site is already in the recently visited list
			if(alreadyInRecentPosition > -1){
				recentContent.splice(alreadyInRecentPosition, 1);
				recentContent.push({'state': stateName, 'title': $scope.pageTitle});
			}
			else if(recentContent.length < 10){
				recentContent.push({'state': stateName, 'title': $scope.pageTitle});
			}
			else {
				recentContent.splice(9, 1);
				recentContent.push({'state': stateName, 'title': $scope.pageTitle});
			}

		},800);		
	});

	// Push changes to db on state change
	// Otherwise we'd be re-directed to home page (for whatever reason)
	$scope.$on('$stateChangeStart', function(){
		recent.set(recentContent);

		// Prevent overwriting when triggered accidentally
		if(list !== null){
			favorites.set(list);
		}
	});
});

// Controller for the favorites tab
App.controller('FavoritesController', function($scope, $ionicSideMenuDelegate, $rootScope) {
	
	$scope.showMenu = function () {
		$ionicSideMenuDelegate.toggleLeft();
	};
	$ionicSideMenuDelegate.canDragContent(true);

	var ref = new Firebase('https://7bits.firebaseio.com/users/' + $rootScope.userId + '/data/favorites');
	ref.on('value', function(snap) {
		$scope.favorites = snap.val();
	});
 
	$scope.data = {
		showDelete: false
	};

	$scope.moveFavorite = function(favorite, fromIndex, toIndex) {
		$scope.favorites.splice(fromIndex, 1);
		$scope.favorites.splice(toIndex, 0, favorite);
	};
 
	$scope.onFavoriteDelete = function(favorite) {
		$scope.favorites.splice($scope.favorites.indexOf(favorite), 1);
	};

	$scope.$on('$stateChangeStart', function(){

		var favoritesClean = [];

		_.each($scope.favorites, function(item){
			favoritesClean.push(_.pick(item, ['title', 'state']));
		});

		ref.set(favoritesClean);

		$scope.data = {
			showDelete: false,
			showReorder: false
		};
	});
});

App.controller('RecentController', function($scope, $ionicSideMenuDelegate, $rootScope, ShowAlert, $translate) {

	// If user is still in trial-phase
	if($rootScope.userData.data.companyId === 1){
		// Tell him that he is
		//ShowAlert.alert($translate.instant('TRIAL_POP_UP_GENERAL_INFO_TITLE'), $translate.instant('TRIAL_POP_UP_GENERAL_INFO_CONTENT'));
	}
	
	$scope.showMenu = function () {
		$ionicSideMenuDelegate.toggleLeft();
	};
	$ionicSideMenuDelegate.canDragContent(true);

	var recent = new Firebase('https://7bits.firebaseio.com/users/' + $rootScope.userId + '/data/recent');
	var recentContent = [];
	recent.once('value', function(snap) {
			
		recentContent = snap.val();
		recentContent = recentContent.reverse();
		$scope.recentContent = recentContent;

		_.each($scope.recentContent, function(content){
			content.state = 'tabs.' + content.state;
		});
	});
});


App.controller('SearchController', function($scope, $ionicSideMenuDelegate, $rootScope, $filter) {
	
	$scope.showMenu = function () {
		$ionicSideMenuDelegate.toggleLeft();
	};
	$ionicSideMenuDelegate.canDragContent(true);

	// Holds all available content with states and it's titles
	var searchIndex = [
		{
			'title': 'CATEGORY_COMMUNICATION_GETTING_COMMITMENT',
			'state': 'tabs.getting-commitment'
		},
		{
			'title': 'CATEGORY_COMMUNICATION_BEFORE_MAKING_A_COMMITMENT',
			'state': 'tabs.before-making-a-commitment'
		},
		{
			'title': 'CATEGORY_COMMUNICATION_BREAKING_A_COMMITMENT_PROFESSIONALLY',
			'state': 'tabs.breaking-a-commitment-professionally'
		},		
		{
			'title': 'CATEGORY_COMMUNICATION_COMMUNICATING_WITH_SOMEONE_WHO_BROKE_THEIR_COMMITMENT',
			'state': 'tabs.communicating-with-someone-who-broke-their-commitment'
		},
		{
			'title': 'CATEGORY_COMMUNICATION_GIVING_CONSTRUCTIVE_FEEDBACK',
			'state': 'tabs.giving-constructive-feedback'
		},
		{
			'title': 'CATEGORY_COMMUNICATION_WRITING_CLEAR_AND_EFFICIENT_EMAIL',
			'state': 'tabs.writing-clear-and-efficient-emails'
		},		
		{
			'title': 'CATEGORY_COMMUNICATION_ASKING_FOR_FEEDBACK',
			'state': 'tabs.asking-for-feedback'
		},
		{
			'title': 'CATEGORY_COMMUNICATION_DEALING_WITH_STRESS',
			'state': 'tabs.dealing-with-stress-performance-anxiety'
		},
		{
			'title': 'CATEGORY_COMMUNICATION_PRESENTING_WITH_IMPACT',
			'state': 'tabs.presenting-with-impact'
		}
	];

	$scope.contentItems = searchIndex;

	$scope.searchGrid = function (termObj) {

		$scope.contentItems = searchIndex;
		while (termObj) {
			var oSearchArray = termObj.split(' ');
			$scope.contentItems = $filter('filter')($scope.contentItems, oSearchArray[0], undefined);
			oSearchArray.shift();
			termObj = (oSearchArray.length !== 0) ? oSearchArray.join(' ') : '';
		}
	};
});

App.controller('HomeController', function($rootScope, ShowAlert, $translate, $timeout) {

	// Wait 50 ms, otherwise it would jump back to source tab	
	$timeout(function() {

		// Stores the timestamp = last use
		var lastUsageRef = new Firebase('https://7bits.firebaseio.com/users/' + $rootScope.userId + '/data');
		var lastUsageCompanyRef = new Firebase('https://7bits.firebaseio.com/companies/' + $rootScope.userData.data.companyId + '/users/data/' + $rootScope.userId + '/data/');

		// One could add 'live'-users that are currently doing an exercise here
		//var connectedRef = new Firebase('https://officelife.firebaseio.com/live');

		// Save date of training in database
		lastUsageRef.update({'lastUsage': Firebase.ServerValue.TIMESTAMP});
		lastUsageCompanyRef.update({'lastUsage': Firebase.ServerValue.TIMESTAMP});
	}, 50);
});

// Cards
App.controller('CardsCtrl', function($scope, TDCardDelegate, $timeout) {

	var cardTypes = [
		{ title: '1. Explain the context briefly and state why the action is important.', content: '"In order to achieve Z I wanted to provide you with the context around the task that needs to be done."' },
		{ title: '2. State your request and leave no ambiguity.', content: '"Can you please do X by Y" (X=Condition of Satisfaction; Y=Deadline)' },
		{ title: '3. Check if clarification is needed.', content: 'a) Deliverables. "Do you understand what I am asking of you?"<br />b) Execution. "Do you need clarification on how the task should be done?”<br />c) Deadline. "Do you understand when I want you to deliver?"' },
		{ title: '4. Clarify resource requirements.', content: '"What do you need from me / us to deliver?"' },
		{ title: '5. Ask for confirmation.', content: 'a) "Can you commit?"<br />b) "Can you deliver in the time given?"' },
		{ title: '', content: 'Listen and restate your commitment request if not understood.' }
	];

	$scope.progressBarMax = cardTypes.length;

	$scope.cards = {
		master: Array.prototype.slice.call(cardTypes, 0),
		active: Array.prototype.slice.call(cardTypes, 0),
		swiped: [],
	};

	$scope.cardDestroyed = function(index) {
		$scope.cards.active.splice(index, 1);
	};

	var i = -1;

	$scope.refreshCards = function() {
		// Set $scope.cards to null so that directive reloads
		$scope.cards.active = null;
		$timeout(function() {
			$scope.cards.active = Array.prototype.slice.call($scope.cards.master, 0);
		});
		$scope.progressBarValue = 0;
		i = -1;
	};

	$scope.cardSwiped = function(index) {
		i = i + 1;
		$scope.progressBarValue = i + 1;
	};

	$scope.goToPreviousCard = function(){
		$scope.progressBarValue = $scope.progressBarValue - 1;
		$scope.cards.active = null;
		$timeout(function() {
			if(i < 0){
				i = 0;
			}
			$scope.cards.active = Array.prototype.slice.call($scope.cards.master, i);
		});
		i = i - 1;
	};
});

App.controller('CardsCtrl2', function($scope, TDCardDelegate, $timeout) {

	var cardTypes = [
		{ title: '1. Rename template file with invoice number and project name.', content: 'Use following format: “#<INVOICE NUMBER>_<YEAR>_<PROJECT NAME>.pdf' },
		{ title: '2. Open invoice template and renew header with invoice number and year.', content: '' },
		{ title: '3. Add date.', content: '' },
		{ title: '4. Add billing address including, contact person and VAT number.', content: 'Before adding VAT number use EU VAT tool to check validity.' },
		{ title: '5. Add project description.', content: 'a) Project name<br />b) City country<br />c) Delivery date or time frame' },
		{ title: '6. Add fees', content: 'a) Design Fees<br />b) Assessment Fees<br />c) Workshop / Facilitation Delivery<br />d) Travel Costs<br />e) VAT 20% OR 0% including comment on why VAT is 0%, if applicable.' },
		{ title: '7. Sign and stamp and send', content: '' },
		{ title: '8. Add invoice to Lighthouse Dashboard for monitoring and cash flow planning ', content: '' }
	];

	$scope.progressBarMax = cardTypes.length;

	$scope.cards = {
		master: Array.prototype.slice.call(cardTypes, 0),
		active: Array.prototype.slice.call(cardTypes, 0),
		swiped: [],
	};

	$scope.cardDestroyed = function(index) {
		$scope.cards.active.splice(index, 1);
	};

	var i = -1;

	$scope.refreshCards = function() {
		// Set $scope.cards to null so that directive reloads
		$scope.cards.active = null;
		$timeout(function() {
			$scope.cards.active = Array.prototype.slice.call($scope.cards.master, 0);
		});
		$scope.progressBarValue = 0;
		i = -1;
	};

	$scope.cardSwiped = function(index) {
		i = i + 1;
		$scope.progressBarValue = i + 1;
	};

	$scope.goToPreviousCard = function(){
		$scope.progressBarValue = $scope.progressBarValue - 1;
		$scope.cards.active = null;
		$timeout(function() {
			if(i < 0){
				i = 0;
			}
			$scope.cards.active = Array.prototype.slice.call($scope.cards.master, i);
		});
		i = i - 1;
	};
});

App.directive('noScroll', function($document) {

	return {
		restrict: 'A',
		link: function($scope, $element, $attr) {
			$document.on('touchmove', function(e) {
				e.preventDefault();
			});
		}
	};
})