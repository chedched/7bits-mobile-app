var App = angular.module('ionicApp', ['ionic', 'firebase', 'ngCordova', 'pascalprecht.translate', 'ionic.contrib.ui.tinderCards2'])

.config(function($stateProvider, $urlRouterProvider) {

	$stateProvider

	/*------------------------------------*\
		#  SIGN UP/SIGN 
	\*------------------------------------*/

	// Sign up
	.state('sign-in-or-sign-up', {
		url: '/sign-in-or-sign-up',
		templateUrl: 'templates/authentication/sign-in-or-sign-up.html',	
	})
	.state('signupchoice', {
		url: '/sign-up-choice',
		templateUrl: 'templates/authentication/sign-up-choice.html',
		controller: 'SignInController'
	})
	.state("signupcode", {
		controller:'SignUpController',
		url:"/sign-up-code",
		templateUrl: "templates/authentication/sign-up-code.html"
	})
	.state("signupcode.confirm", {
		controller:'SignUpController',
		parent: 'signupcode',
		url:"/sign-up-code-confirm",
		templateUrl: 'templates/authentication/sign-up-code-confirm.html'
	})
	.state('signupmail', {
		url: '/sign-up',
		templateUrl: 'templates/authentication/sign-up-mail.html',
		controller: 'SignUpController'
	})

	// Sign in
	.state('signinchoice', {
		url: '/sign-in-choice',
		templateUrl: 'templates/authentication/sign-in-choice.html',
		controller: 'SignInController'
	})
	.state("signincode", {
		controller:'SignInController',
		url:"/sign-in-code",
		templateUrl: "templates/authentication/sign-in-code.html"
	})
	.state("signincode.confirm", {
		controller:'SignInController',
		parent: 'signincode',
		url:"/sign-in-code-confirm",
		templateUrl: 'templates/authentication/sign-in-code-confirm.html'
	})
	.state('signinmail', {
		url: '/sign-in-mail',
		templateUrl: 'templates/authentication/sign-in-mail.html',
		controller: 'SignInController'
	})

	/*------------------------------------*\
		#  TABS
	\*------------------------------------*/

	.state('tabs', {
		url: "/tab",
		abstract: true,
		templateUrl: "templates/tabs.html"
	})
	.state('tabs.home', {
		url: "/home",
		views: {
			'home-tab': {
				templateUrl: "templates/home.html",
				controller: 'NavCtrl'
			}
		},
		cache: false
	})
	.state('tabs.favorite', {
		url: "/favorite",
		views: {
			'favorite-tab': {
				templateUrl: "templates/favorite.html",
				controller: 'FavoritesController'
			}
		}
	})
	.state('tabs.search', {
		url: "/search",
		views: {
			'search-tab': {
				templateUrl: "templates/search.html",
				controller: 'SearchController'
			}
		}
	})
	.state('tabs.recent', {
		url: "/recent",
		views: {
			'recent-tab': {
				templateUrl: "templates/recent.html",
				controller: 'RecentController'
			}
		}
	})
	.state('tabs.settingscontainer', {
		url: "/settingscontainer"
	})
	

	/*------------------------------------*\
		#  CATEGORIES
	\*------------------------------------*/

	// Communication
	.state('tabs.communication', {
		url: "/communication",
		views: {
			'home-tab': {
				templateUrl: "templates/categories/communication/communication.html",
			}
		},
	})
	.state('tabs.before-making-a-commitment', {
		url: "/before-making-a-commitment",
		views: {
			'home-tab': {
				templateUrl: "templates/categories/communication/before-making-a-commitment.html",
				controller: 'ContentController'
			}
		}
	})
	.state('tabs.getting-commitment', {
		url: "/getting-commitment",
		views: {
			'home-tab': {
				templateUrl: "templates/categories/communication/getting-commitment.html",
				controller: 'ContentController'
			}
		}
	})
	.state('tabs.breaking-a-commitment-professionally', {
		url: "/breaking-a-commitment-professionally",
		views: {
			'home-tab': {
				templateUrl: "templates/categories/communication/breaking-a-commitment-professionally.html",
				controller: 'ContentController'
			}
		}
	})
	.state('tabs.communicating-with-someone-who-broke-their-commitment', {
		url: "/communicating-with-someone-who-broke-their-commitment",
		views: {
			'home-tab': {
				templateUrl: "templates/categories/communication/communicating-with-someone-who-broke-their-commitment.html",
				controller: 'ContentController'
			}
		}
	})
	.state('tabs.giving-constructive-feedback', {
		url: "/giving-constructive-feedback",
		views: {
			'home-tab': {
				templateUrl: "templates/categories/communication/giving-constructive-feedback.html",
				controller: 'ContentController'
			}
		}
	})
	.state('tabs.writing-clear-and-efficient-emails', {
		url: "/writing-clear-and-efficient-emails",
		views: {
			'home-tab': {
				templateUrl: "templates/categories/communication/writing-clear-and-efficient-emails.html",
				controller: 'ContentController'
			}
		}
	})
	.state('tabs.asking-for-feedback', {
		url: "/asking-for-feedback",
		views: {
			'home-tab': {
				templateUrl: "templates/categories/communication/asking-for-feedback.html",
				controller: 'ContentController'
			}
		}
	})
	.state('tabs.dealing-with-stress-performance-anxiety', {
		url: "/dealing-with-stress-performance-anxiety",
		views: {
			'home-tab': {
				templateUrl: "templates/categories/communication/dealing-with-stress-performance-anxiety.html",
				controller: 'ContentController'
			}
		}
	})
	.state('tabs.presenting-with-impact', {
		url: "/presenting-with-impact",
		views: {
			'home-tab': {
				templateUrl: "templates/categories/communication/presenting-with-impact.html",
				controller: 'ContentController'
			}
		}
	})
	
	// Team work
	.state('tabs.teamwork', {
		url: "/teamwork",
		views: {
			'home-tab': {
				templateUrl: "templates/categories/teamwork/teamwork.html",
			}
		}
	})

	// Leadership
	.state('tabs.leadership', {
		url: "/leadership",
		views: {
			'home-tab': {
				templateUrl: "templates/categories/leadership/leadership.html",
			}
		}
	})

	// Effectiveness
	.state('tabs.effectiveness', {
		url: "/effectiveness",
		views: {
			'home-tab': {
				templateUrl: "templates/categories/effectiveness/effectiveness.html",
			}
		}
	})

	// Customer support
	.state('tabs.customersupport', {
		url: "/customersupport",
		views: {
			'home-tab': {
				templateUrl: "templates/categories/customersupport/customersupport.html",
			}
		}
	})

	// Change
	.state('tabs.change', {
		url: "/change",
		views: {
			'home-tab': {
				templateUrl: "templates/categories/change/change.html",
			}
		}
	})

	// Conflict
	.state('tabs.conflict', {
		url: "/conflict",
		views: {
			'home-tab': {
				templateUrl: "templates/categories/conflict/conflict.html",
			}
		}
	})

	// Presentation
	.state('tabs.presentation', {
		url: "/presentation",
		views: {
			'home-tab': {
				templateUrl: "templates/categories/presentation/presentation.html",
			}
		}
	})

	// Swipe
	.state('tabs.swipe', {
		url: "/swipe",
		views: {
			'home-tab': {
				templateUrl: "templates/swipe.html",
			}
		},
	})
	.state('tabs.swipe2', {
		url: "/swipe2",
		views: {
			'home-tab': {
				templateUrl: "templates/swipe2.html",
			}
		},
	})

	/*------------------------------------*\
		#  SETTINGS
	\*------------------------------------*/


	.state('tabs.account', {
		url: "/account",
		views: {
			'settings-tab': {
				templateUrl: "templates/settings/account.html"
			}
		}
	})
	.state('tabs.change-password', {
		url: "/change-password",
		views: {
			'settings-tab': {
				templateUrl: "templates/settings/change-password.html",
				controller: 'ChangePasswordCtrl'
			}
		}
	})
	.state('tabs.subscription', {
		url: "/subscription",
		views: {
			'settings-tab': {
				templateUrl: "templates/settings/subscription.html",
				controller: 'SubscriptionController'
			}
		}
	})
	/*.state('tabs.getpremium', {
		url: "/get-premium-account",
		views: {
			'settings-tab': {
				templateUrl: "templates/settings/get-premium.html",
				controller: 'SubscriptionController'
			}
		}
	})*/
	.state('tabs.unlockwithcode', {
		url: "/unlock-with-code",
		views: {
			'settings-tab': {
				templateUrl: "templates/settings/unlock-with-code.html",
				controller: 'SubscriptionController'
			}
		}
	})
	.state('tabs.deleteaccount', {
		url: "/account",
		views: {
			'settings-tab': {
				templateUrl: "templates/settings/delete-account.html"
			}
		}
	})
	.state('tabs.settings', {
		url: "/settings",
		views: {
			'settings-tab': {
				templateUrl: "templates/settings/settings.html"
			}
		}
	})
	.state('tabs.feedback', {
		url: "/feedback",
		views: {
			'settings-tab': {
				templateUrl: "templates/settings/feedback-contact.html"
			}
		}
	})

	.state('tabs.help', {
		url: "/help",
		views: {
			'settings-tab': {
				templateUrl: "templates/settings/help-center.html"
			}
		}
	})
	.state('tabs.privacy', {
		url: "/privacy",
		views: {
			'settings-tab': {
				templateUrl: "templates/settings/privacy.html"
			}
		}
	})
	.state('forgotpassword', {
		url: '/forgot-password',
		templateUrl: 'templates/forgot-password.html',
		controller: 'ResetPasswordCtrl'
	})
	
	
	.state('tabs.change-mail', {
		url: "/change-mail",
		views: {
			'home-tab': {
				templateUrl: "templates/change-mail.html",
				controller: 'ChangeMailCtrl'
			}
		}
	})
	
	.state('tabs.sign-out', {
		url: "/sign-out",
		views: {
			'home-tab': {
				templateUrl: "templates/sign-out.html",
				controller: 'SignOutCtrl'
			}
		}
	});

	$urlRouterProvider.otherwise('/sign-in-or-sign-up');

})

// Set current state (used for displaying settings & toggling settings by swiping)
.run(function ($state,$rootScope) {
	$rootScope.$state = $state;
});