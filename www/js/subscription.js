var App = angular.module('ionicApp');

App.controller('SubscriptionController', function($scope, $rootScope, $sce, $translate) {

	// Get difference from expiry-date to now
	var dayDifference = moment($rootScope.userData.expiry).diff(moment(), 'days');

	if(dayDifference < 0){
		$scope.expiry = $translate.instant('ACCOUNT_SUBSCRIPTION_EXPIRY_EXPIRED');
	}
	else {
		if($rootScope.userData.company === 'Trial Users'){
			$scope.subscriptionType = $sce.trustAsHtml($translate.instant('ACCOUNT_SUBSCRIPTION_INFO_TRIAL'));
		}
		else if($rootScope.userData.company === 'Premium Users'){
			$scope.subscriptionType = $sce.trustAsHtml($translate.instant('ACCOUNT_SUBSCRIPTION_INFO_PREMIUM'));
		}
		else {
			$scope.subscriptionType = $sce.trustAsHtml($translate.instant('ACCOUNT_SUBSCRIPTION_INFO_COMPANY'));
		}

		// Print remaining time
		$scope.expiry = $translate.instant('ACCOUNT_SUBSCRIPTION_EXPIRY_EXPIRY_IN') + moment().to($rootScope.userData.expiry) + '.';
	}
});

// Sign up functionality
App.controller('UnlockCodeController', function($scope, $rootScope, $sce, $translate) {

});