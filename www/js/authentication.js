var App = angular.module('ionicApp');

App.factory('Authentication', ['$firebaseAuth', function($firebaseAuth) {
	
	var Authentication = {};
	var firebaseObjectUrl = new Firebase('https://7bits.firebaseio.com/');

	Authentication.firebaseAuth = function(){
		return ($firebaseAuth(firebaseObjectUrl));
	};

	Authentication.url = function(){
		return firebaseObjectUrl;
	};

	return Authentication;
}]);


App.controller('SignUpController', function($scope, $rootScope, $state, $sce, $ionicSideMenuDelegate, $translate, Authentication, ShowAlert, IdentifyWithCode, User) {

	$ionicSideMenuDelegate.canDragContent(false);
	$scope.auth = Authentication.firebaseAuth();

	/*------------------------------------*\
		#  CODE
	\*------------------------------------*/

	// Show code input field when signing up with code
	$scope.codeInput = true;

	$scope.checkCode = function(code){
		IdentifyWithCode.getInfo(code,
			function(data){

				// If code is valid
				if(data.val() !== null){
					$scope.codeInput = false;
					$state.go('signupcode.confirm');
					$scope.mailAddress = data.val().mail;
					$scope.company = data.val().company;
					$scope.companyId = data.val().companyId;
				}

				// If code is invalid
				else {
					ShowAlert.alert($translate.instant('SIGN_UP_WITH_CODE_POPUP_FAILURE'));
				}
			}
		);
	};

	$scope.signUpWithCode = function(password){
	
		$scope.auth.$createUser({
			email: $scope.mailAddress,
			password: password
		})
		.then(function(userData) {
			userInfos = userData;
			return $scope.auth.$authWithPassword({
				email: $scope.mailAddress,
				password: password
			});
		})
		.then(function(authData) {
			var created = Firebase.ServerValue.TIMESTAMP;
			
			// Add data to user's node (is not 'account'-node!)
			User.addToUsersNode(authData.uid, authData.password.email, 'first name', 'last name', created, 10, $scope.company, $scope.companyId);

			// Add data to companies-node
			User.addToCompaniesNode(authData.uid, authData.password.email, 'first name', 'last name', created, 10, $scope.companyId, $scope.company);
			ShowAlert.alert($translate.instant('SIGN_UP_WITH_CODE_POPUP_SUCCESS'));

			// Get data of a user
			// = Data from users' node, NOT account
			User.getData(authData.uid, function(data){
				$rootScope.userId = authData.uid;
				$rootScope.userData = data.val();
								
				// If user is still in trial-phase
				if(data.val().data.companyId === 1){
					ShowAlert.alert($translate.instant('TRIAL_POP_UP_GENERAL_INFO_TITLE'), $translate.instant('TRIAL_POP_UP_GENERAL_INFO_CONTENT'));
				}
				$state.go('tabs.home');
			});
		})
		.catch(function(error) {
			switch (error.code) {
				case 'EMAIL_TAKEN':
					ShowAlert.alert($translate.instant('SIGN_UP_POPUP_FAILURE_EMAIL_TAKEN_TITLE'), $translate.instant('SIGN_UP_POPUP_FAILURE_EMAIL_TAKEN_CONTENT_1') + mailAddress + $translate.instant('SIGN_UP_POPUP_FAILURE_EMAIL_TAKEN_CONTENT_2'));
					break;
				case 'INVALID_EMAIL':
					ShowAlert.alert($translate.instant('SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_TITLE'), $translate.instant('SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_CONTENT_1') + mailAddress + $translate.instant('SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_CONTENT_2'));
					break;
				case 'NETWORK_ERROR':
					ShowAlert.alert($translate.instant('SIGN_UP_POPUP_FAILURE_NETWORK_ERROR_TITLE'), $translate.instant('SIGN_UP_POPUP_FAILURE_NETWORK_ERROR_CONTENT'));
					break;
			default:
				ShowAlert.alert($translate.instant('SIGN_UP_POPUP_FAILURE_DEFAULT_TITLE'), $translate.instant('SIGN_UP_POPUP_FAILURE_DEFAULT_CONTENT'));
			}
		});
	};

	/*------------------------------------*\
		#  MAIL
	\*------------------------------------*/

	$scope.signUp = function(mailAddress, password){

		// Confirm mail address in a popup
		var popUp = ShowAlert.confirm($translate.instant('SIGN_UP_POPUP_TITLE'), $translate.instant('SIGN_UP_POPUP_CONTENT_1') + mailAddress + $translate.instant('SIGN_UP_POPUP_CONTENT_2'), $translate.instant('SIGN_UP_POPUP_CANCEL'), $translate.instant('SIGN_UP_POPUP_CONFIRM'));

		// If user confirms
		popUp.then(function(res) {

			if(res) {
				$scope.auth.$createUser({
					email: mailAddress,
					password: password
				})
				.then(function(userData) {
					return $scope.auth.$authWithPassword({
						email: mailAddress,
						password: password
					});
				})
				.then(function(authData) {
					var created = Firebase.ServerValue.TIMESTAMP;
					var expiry = moment().add(3, 'days');
					expiry = moment(expiry).valueOf();

					// Add data to user's node (is not 'account'-node!)
					User.addToUsersNode(authData.uid, authData.password.email, 'first name', 'last name', created, 10, 'Trial Users', 1);

					// Add data to companies-node
					User.addToCompaniesNode(authData.uid, authData.password.email, 'first name', 'last name', created, 10, 1, 'Trial Users');
					ShowAlert.alert($translate.instant('TRIAL_POP_UP_GENERAL_INFO_TITLE'), $translate.instant('TRIAL_POP_UP_GENERAL_INFO_CONTENT'));

					// Get data of a user
					// = Data from users' node, NOT account
					User.getData(authData.uid, function(data){
						$rootScope.userId = authData.uid;
						$rootScope.userData = data.val();
										
						// If user is still in trial-phase
						if(data.val().data.companyId === 1){
							ShowAlert.alert($translate.instant('TRIAL_POP_UP_GENERAL_INFO_TITLE'), $translate.instant('TRIAL_POP_UP_GENERAL_INFO_CONTENT'));
						}
						$state.go('tabs.home');
					});
				})
				.catch(function(error) {
					switch (error.code) {
						case 'EMAIL_TAKEN':
							ShowAlert.alert($translate.instant('SIGN_UP_POPUP_FAILURE_EMAIL_TAKEN_TITLE'), $translate.instant('SIGN_UP_POPUP_FAILURE_EMAIL_TAKEN_CONTENT_1') + mailAddress + $translate.instant('SIGN_UP_POPUP_FAILURE_EMAIL_TAKEN_CONTENT_2'));
							break;
						case 'INVALID_EMAIL':
							ShowAlert.alert($translate.instant('SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_TITLE'), $translate.instant('SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_CONTENT_1') + mailAddress + $translate.instant('SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_CONTENT_2'));
							break;
						case 'NETWORK_ERROR':
							ShowAlert.alert($translate.instant('SIGN_UP_POPUP_FAILURE_NETWORK_ERROR_TITLE'), $translate.instant('SIGN_UP_POPUP_FAILURE_NETWORK_ERROR_CONTENT'));
							break;
					default:
						ShowAlert.alert($translate.instant('SIGN_UP_POPUP_FAILURE_DEFAULT_TITLE'), $translate.instant('SIGN_UP_POPUP_FAILURE_DEFAULT_CONTENT'));
					}
				});
			} 
		});
	};
});

App.controller('SignInController', function($scope, $rootScope, $state, $ionicSideMenuDelegate, $translate, Authentication, ShowAlert, User, IdentifyWithCode) {
	
	// Show code input field
	$scope.codeInput = true;

	$scope.auth = Authentication.firebaseAuth();
	$ionicSideMenuDelegate.canDragContent(false);

	/*------------------------------------*\
		#  CODE
	\*------------------------------------*/

	$scope.checkCode = function(code){
		IdentifyWithCode.getInfo(code,
			function(data){

				if(data.val() !== null){
					$scope.codeInput = false;
					$state.go('signincode.confirm');
					$scope.mailAddress = data.val().mail;
				}
				else {
					ShowAlert.alert($translate.instant('SIGN_UP_WITH_CODE_POPUP_FAILURE'));
				}
			}
		);
	};

	/*------------------------------------*\
		#  MAIL
	\*------------------------------------*/

	$scope.signInMail = function(user, password) {

		// If no user defined, abandon
		// As send button is disabled before input, this should never trigger
		if (!user) {
			ShowAlert.alert($translate.instant('SIGN_IN_POPUP_NO_USERNAME_PROVIDED_TITLE'), $translate.instant('SIGN_IN_POPUP_NO_USERNAME_PROVIDED_CONTENT'));
			return false;
		}
		
		// If no password defined, abandon
		// As send button is disabled before input, this should never trigger
		if (!password) {
			ShowAlert.alert($translate.instant('SIGN_IN_POPUP_NO_PASSWORD_PROVIDED_TITLE'), $translate.instant('SIGN_IN_POPUP_NO_PASSWORD_PROVIDED_CONTENT'));
			return false;
		}

		$scope.auth.$authWithPassword({
			email 	 : user,
			password : password
		})
		.then(function(authData) {

			// Store each connection instance separately, since multiple devices or browser tabs could be connected
			// Any time that connectionsRef's value is null (i.e. has no children), user is logged out
			// Entry in user's database entry
			var userConnectionsRef = new Firebase('https://7bits.firebaseio.com/users/' + authData.uid + '/connections');

			// Entry in 'connections'-property of database
			// = This is a list of all users currently connected
			var generalConnectionsRef = new Firebase('https://7bits.firebaseio.com/connected');

			// Stores the timestamp of my last disconnect (the last time I was seen online)
			var lastOnlineRef = new Firebase('https://7bits.firebaseio.com/users/' + authData.uid + '/data/lastOnline');
			var connectedRef = new Firebase('https://7bits.firebaseio.com/.info/connected');

			connectedRef.on('value', function(snap) {
					
				if (snap.val() === true) {
					// We're connected (or reconnected)! Do anything here that should happen only if online (or on reconnect)
					// add this device to my connections list
					// this value could contain info about the device or a timestamp too
						
					// Add device to list user's database entry
					var userConnection = userConnectionsRef.push(true);

					// Add device to general database entry (=list of all currently connected users)
					var generalConnection = generalConnectionsRef.push({'mobile': authData.uid});

					// When user disconnects, remove this device
					userConnection.onDisconnect().remove();
					generalConnection.onDisconnect().remove();

					// When user disconnects, update the last time he or she was seen online
					lastOnlineRef.onDisconnect().set(Firebase.ServerValue.TIMESTAMP);
				}
			});

			$scope.mailAddress = authData.password.email;

			// Get data of a user
			// = Data from users' node, NOT account
			User.getData(authData.uid, function(data){
				$rootScope.userId = authData.uid;
				$rootScope.userData = data.val();
				$state.go('tabs.home');
			});
		})
		.catch(function(error) {
			switch (error.code) {
				case 'INVALID_EMAIL':
					ShowAlert.alert($translate.instant('SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_TITLE'), $translate.instant('SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_CONTENT_1') + user + $translate.instant('SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_CONTENT_2'));
					break;
				case 'INVALID_PASSWORD':
					ShowAlert.alert($translate.instant('SIGN_IN_POPUP_FAILURE_INVALID_PASSWORD_TITLE'), $translate.instant('SIGN_IN_POPUP_FAILURE_INVALID_PASSWORD_CONTENT'));
					break;
				case 'INVALID_USER':
					ShowAlert.alert($translate.instant('SIGN_IN_POPUP_FAILURE_INVALID_USER_TITLE'), $translate.instant('SIGN_IN_POPUP_FAILURE_INVALID_USER_CONTENT'));
					break;
				case 'NETWORK_ERROR':
					ShowAlert.alert($translate.instant('SIGN_UP_POPUP_FAILURE_NETWORK_ERROR_TITLE'), $translate.instant('SIGN_UP_POPUP_FAILURE_NETWORK_ERROR_CONTENT'));
					break;
				default:
					ShowAlert.alert($translate.instant('SIGN_UP_POPUP_FAILURE_DEFAULT_TITLE'), $translate.instant('SIGN_UP_POPUP_FAILURE_DEFAULT_CONTENT'));
			}
		});
	};

	/*------------------------------------*\
		#  FACEBOOK
	\*------------------------------------*/
	
	$scope.signInFacebook = function(){

		var created = Firebase.ServerValue.TIMESTAMP;
		var expiry = moment().add(3, 'days');
		expiry = moment(expiry).valueOf();

		Authentication.url().authWithOAuthPopup("facebook", function(error, authData) {

			if(error){
				console.log(error);
			}
			else {

				// Store each connection instance separately, since multiple devices or browser tabs could be connected
				// Any time that connectionsRef's value is null (i.e. has no children), user is logged out
				// Entry in user's database entry
				var userConnectionsRef = new Firebase('https://officelife.firebaseio.com/users/' + authData.uid + '/connections');

				// Entry in 'connections'-property of database
				// = This is a list of all users currently connected
				var generalConnectionsRef = new Firebase('https://officelife.firebaseio.com/connected');

				// Stores the timestamp of my last disconnect (the last time I was seen online)
				var lastOnlineRef = new Firebase('https://officelife.firebaseio.com/users/' + authData.uid + '/data/lastOnline');
				var connectedRef = new Firebase('https://officelife.firebaseio.com/.info/connected');

				connectedRef.on('value', function(snap) {
						
					if (snap.val() === true) {
						// We're connected (or reconnected)! Do anything here that should happen only if online (or on reconnect)
						// add this device to my connections list
						// this value could contain info about the device or a timestamp too
							
						// Add device to list user's database entry
						var userConnection = userConnectionsRef.push(true);

						// Add device to general database entry (=list of all currently connected users)
						var generalConnection = generalConnectionsRef.push({'mobile': authData.uid});

						// When user disconnects, remove this device
						userConnection.onDisconnect().remove();
						generalConnection.onDisconnect().remove();

						// When user disconnects, update the last time he or she was seen online
						lastOnlineRef.onDisconnect().set(Firebase.ServerValue.TIMESTAMP);
					}
				});

				// Get data of a user
				// = Data from users' node, NOT account
				User.getData(authData.uid, function(data){
					$rootScope.userId = authData.uid;
					$rootScope.userData = data.val();

					if(data.val() === null){
						$rootScope.userData = {};
						$rootScope.userData.data.account = 'trial';
						$rootScope.userData.data.active = true;
						$rootScope.userData.data.company = 'Trial Users';
						$rootScope.userData.data.created = moment().now();
						$rootScope.userData.data.mail = authData.facebook.email;
						$rootScope.userData.data.role = 10;

						// Add data to user's node (is not 'account'-node!)
						User.addToUsersNode(authData.uid, authData.facebook.email, created, expiry, 'facebook', 'user', 'Trial Users', 1, 'trial');

						// Add data to companies-node
						User.addToCompaniesNode(authData.uid, authData.facebook.email, created, expiry, 'facebook', 'user', 'Trial Users', 1, 'trial');
						ShowAlert.alert($translate.instant('TRIAL_POP_UP_GENERAL_INFO_TITLE'), $translate.instant('TRIAL_POP_UP_GENERAL_INFO_CONTENT'));
						$state.go('tabs.home');
					}
				});
			}	
		}, 

		// Data that is being fetched from facebook
		{
			scope: "email"
		});
	};

	/*------------------------------------*\
		#  GOOGLE
	\*------------------------------------*/
	
	$scope.signInGoogle = function(){
		
		var created = Firebase.ServerValue.TIMESTAMP;
		var expiry = moment().add(3, 'days');
		expiry = moment(expiry).valueOf();

		Authentication.url().authWithOAuthPopup("google", function(error, authData) {
			if(error){
				console.log(error);
			}
			else {

				// Store each connection instance separately, since multiple devices or browser tabs could be connected
				// Any time that connectionsRef's value is null (i.e. has no children), user is logged out
				// Entry in user's database entry
				var userConnectionsRef = new Firebase('https://officelife.firebaseio.com/users/' + authData.uid + '/connections');

				// Entry in 'connections'-property of database
				// = This is a list of all users currently connected
				var generalConnectionsRef = new Firebase('https://officelife.firebaseio.com/connected');

				// Stores the timestamp of my last disconnect (the last time I was seen online)
				var lastOnlineRef = new Firebase('https://officelife.firebaseio.com/users/' + authData.uid + '/data/lastOnline');
				var connectedRef = new Firebase('https://officelife.firebaseio.com/.info/connected');

				connectedRef.on('value', function(snap) {
						
					if (snap.val() === true) {
						// We're connected (or reconnected)! Do anything here that should happen only if online (or on reconnect)
						// add this device to my connections list
						// this value could contain info about the device or a timestamp too
							
						// Add device to list user's database entry
						var userConnection = userConnectionsRef.push(true);

						// Add device to general database entry (=list of all currently connected users)
						var generalConnection = generalConnectionsRef.push({'mobile': authData.uid});

						// When user disconnects, remove this device
						userConnection.onDisconnect().remove();
						generalConnection.onDisconnect().remove();

						// When user disconnects, update the last time he or she was seen online
						lastOnlineRef.onDisconnect().set(Firebase.ServerValue.TIMESTAMP);
					}
				});

				// Get data of a user
				// = Data from users' node, NOT account
				User.getData(authData.uid, function(data){
					$rootScope.userId = authData.uid;
					$rootScope.userData = data.val();

					// If user doesn't exist = signs in for the very first time
					if(data.val() === null){
						$rootScope.userData = {};
						$rootScope.userData.data.account = 'trial';
						$rootScope.userData.data.active = true;
						$rootScope.userData.data.company = 'Trial Users';
						$rootScope.userData.data.created = moment().now();
						$rootScope.userData.data.mail = authData.facebook.email;
						$rootScope.userData.data.role = 10;

						User.addToUsersNode(authData.uid, authData.google.email, created, expiry, 'google', 'user', 'Trial Users', 1, 'trial');
						User.addToCompaniesNode(authData.uid, authData.google.email, created, expiry, 'google', 'user', 'Trial Users', 1, 'trial');
						ShowAlert.alert($translate.instant('TRIAL_POP_UP_GENERAL_INFO_TITLE'), $translate.instant('TRIAL_POP_UP_GENERAL_INFO_CONTENT'));
						$state.go('tabs.home');
					}
				});
			}
		}, 

		// Data that is being fetched from google
		{
			scope: "email"
		});
	};
});


App.controller('ResetPasswordCtrl', function($scope, $ionicSideMenuDelegate, $ionicPopup, $translate, Authentication, ShowAlert) {

	$scope.auth = Authentication.firebaseAuth();
	$ionicSideMenuDelegate.canDragContent(false);
	
	$scope.resetPassword = function() {
				
		$scope.auth.$resetPassword({
			email: $scope.email
		})
		.then(function() {
			ShowAlert.alert($translate.instant('FORGOT_PASSWORD_POPUP_SUCCESS_TITLE'), $translate.instant('FORGOT_PASSWORD_POPUP_SUCCESS_CONTENT_1') + $scope.email + $translate.instant('FORGOT_PASSWORD_POPUP_SUCCESS_CONTENT_2'));
		})
		.catch(function(error) {
			switch (error.code) {
				case 'INVALID_EMAIL':
					ShowAlert.alert($translate.instant('SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_TITLE'), $translate.instant('SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_CONTENT_1') + user + $translate.instant('SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_CONTENT_2'));
					break;
				case 'INVALID_USER':
					ShowAlert.alert($translate.instant('SIGN_IN_POPUP_FAILURE_INVALID_USER_TITLE'), $translate.instant('SIGN_IN_POPUP_FAILURE_INVALID_USER_CONTENT'));
					break;
				case 'NETWORK_ERROR':
					ShowAlert.alert($translate.instant('SIGN_UP_POPUP_FAILURE_NETWORK_ERROR_TITLE'), $translate.instant('SIGN_UP_POPUP_FAILURE_NETWORK_ERROR_CONTENT'));
					break;
				default:
					ShowAlert.alert($translate.instant('SIGN_UP_POPUP_FAILURE_DEFAULT_TITLE'), $translate.instant('SIGN_UP_POPUP_FAILURE_DEFAULT_CONTENT'));
			}	
		});
	};
});

App.controller('ChangeMailCtrl', function($scope, $translate, $state, Authentication, ShowAlert) {

	$scope.auth = Authentication.firebaseAuth;
	$scope.user = $scope.auth.$getAuth();
	$scope.newMail = {};

	$scope.changeMail = function(){

		var firebase = Authentication.url();

		firebase.changeEmail({
			oldEmail: $scope.user.password.email,
			newEmail: $scope.newMail.mail,
			password: $scope.newMail.confirmPassword
		}, function(error) {

			// If failure
			if(error) {
				switch (error.code) {
					case "INVALID_PASSWORD":
						ShowAlert.alert($translate.instant('CHANGE_MAIL_POPUP_FAILURE_WRONG_PASSWORT_TITLE'), $translate.instant('CHANGE_MAIL_POPUP_FAILURE_WRONG_PASSWORT_CONTENT'));
					break;
					case "INVALID_USER":
						ShowAlert.alert($translate.instant('CHANGE_MAIL_POPUP_FAILURE_DEFAULT_TITLE'), $translate.instant('CHANGE_MAIL_POPUP_FAILURE_DEFAULT_ERROR'));
					break;
					default:
						ShowAlert.alert($translate.instant('CHANGE_MAIL_POPUP_FAILURE_DEFAULT_TITLE'), $translate.instant('CHANGE_MAIL_POPUP_FAILURE_DEFAULT_ERROR'));
				}
			}
			else {
				ShowAlert.alert($translate.instant('CHANGE_MAIL_POPUP_SUCCESS_CHANGE_MAIL_TITLE'), $translate.instant('CHANGE_MAIL_POPUP_SUCCESS_CHANGE_MAIL_CONTENT'));
				$state.go('tabs.home');
			}
		});
	};
});

App.controller('ChangePasswordCtrl', function($scope, $translate, $state, Authentication, ShowAlert) {
	
	$scope.auth = Authentication.firebaseAuth();
	$scope.user = $scope.auth.$getAuth();

	$scope.changePassword = function(oldPassword, newPassword){

		$scope.auth.$changePassword({
			email: $scope.user.password.email,
			oldPassword: oldPassword,
			newPassword: newPassword
		})
		.then(function() {
			ShowAlert.alert('Wohoo', 'You changed your password like a pro.');
			$state.go('tabs.home');
		})
		.catch(function(error) {
			switch (error.code) {
				case 'INVALID_EMAIL':
					ShowAlert.alert($translate.instant('SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_TITLE'), $translate.instant('SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_CONTENT_1') + user + $translate.instant('SIGN_UP_POPUP_FAILURE_EMAIL_INVALID_CONTENT_2'));
					break;
				case 'INVALID_PASSWORD':
					ShowAlert.alert($translate.instant('SIGN_IN_POPUP_FAILURE_INVALID_PASSWORD_TITLE'), $translate.instant('SIGN_IN_POPUP_FAILURE_INVALID_PASSWORD_CONTENT'));
					break;
				case 'INVALID_USER':
					ShowAlert.alert($translate.instant('SIGN_IN_POPUP_FAILURE_INVALID_USER_TITLE'), $translate.instant('SIGN_IN_POPUP_FAILURE_INVALID_USER_CONTENT'));
					break;
				case 'NETWORK_ERROR':
					ShowAlert.alert($translate.instant('SIGN_UP_POPUP_FAILURE_NETWORK_ERROR_TITLE'), $translate.instant('SIGN_UP_POPUP_FAILURE_NETWORK_ERROR_CONTENT'));
					break;
				default:
					ShowAlert.alert($translate.instant('SIGN_UP_POPUP_FAILURE_DEFAULT_TITLE'), $translate.instant('SIGN_UP_POPUP_FAILURE_DEFAULT_CONTENT'));
			}
		});
	};
});

App.controller('SignOutCtrl', function($scope, $state, Authentication) {

	$scope.auth = Authentication.firebaseAuth();

	$scope.logout = function() {

		// Manually disconnect from database
		//Firebase.goOffline();
		$scope.auth.$unauth();
		$state.go('sign-in-or-sign-up');
	};
});

App.factory('User', function ($firebaseArray) {
	
	var user = {};
	var url = new Firebase('https://7bits.firebaseio.com');

	user.url = function() {
		return url;
	};

	// Adds user to users' node
	// This is just for listing users etc, it's NOT the actual account that enables login (partner of makeAccount)
	user.addToUsersNode = function(uid, mail, firstname, lastname, created, role, companyName, companyId){
		
		var data = {};
		data.mail = mail;
		data.firstname = firstname;
		data.lastname = lastname;				
		data.created = 	created;	
		data.provider = 'mail';			
		data.role = role;				
		data.active = true;	
		data.company = companyName;				
		data.companyId = companyId;
		data.account = 'company';
						
		return url.child('users').child(uid).child('data').setWithPriority(data, data.mail);
	};

	// Adds user to companies' node
	// Every company-node has a sub-node with all users that belong to company
	user.addToCompaniesNode = function(uid, mail, firstname, lastname, created, role, companyId, companyName){
		
		var data = {};
		data.mail = mail;
		data.firstname = firstname;
		data.lastname = lastname;
		data.created = 	created;
		data.provider = 'mail';	
		data.role = 	role;	
		data.active = true;
		data.company = companyName;				
		data.companyId = companyId;
		data.account = 'company';

		return url.child('companies').child(companyId).child('users').child('data').child(uid).child('data').setWithPriority(data, data.mail);
	};

	// Get data of a user
	// = Data from users' node, NOT account
	user.getData = function(uid, success){
		url.child('users').child(uid).on('value', success);
	};

	return user;
});

App.factory('IdentifyWithCode', function ($firebaseArray) {

	var url = new Firebase('https://7bits.firebaseio.com');
	var user = {};

	user.getInfo = function(code, success) {
		url.child('codes').child(code).on('value', success);
	};

	return user;
});